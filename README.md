# Book Club Project

## Introduction

This project is a Book Club web application built using pure HTML, CSS, and JavaScript. It leverages Supabase for database management and user authentication, and Imgur for image hosting. The application allows users to register, log in, add books, view books, rate and comment on books, and participate in a chat room.

## Online Linke of project
**[Access the live project here](https://bookclub-radiophp-c74e4b79ee93cb54f381aed4a661eeafbb5db76297024.gitlab.io/login.html)**


## Features

1. **User Authentication**: Users can register and log in to the application.
2. **Book Management**: Users can add new books with images, view a list of books, and see detailed information about each book.
3. **Rating and Comments**: Users can rate books and add comments. Each comment shows the user’s email, comment, rating, and the date it was posted.
4. **Chat Room**: A public chat room where users can send messages and images.

### Screenshots

#### Login Page
<div align="center">
    <img src="https://gitlab.com/radiophp/bookclub/-/raw/main/Screenshot-LOGIN.png?ref_type=heads&inline=false" alt="Login Page" width="800px">
</div>

#### Register Page
<div align="center">
    <img src="https://gitlab.com/radiophp/bookclub/-/raw/main/Screenshot-REGISTER.png?ref_type=heads&inline=false" alt="Register Page" width="800px">
</div>

#### Add Book Page
<div align="center">
    <img src="https://gitlab.com/radiophp/bookclub/-/raw/main/Screenshot-ADD-BOOK.png?ref_type=heads&inline=false" alt="Add Book Page" width="800px">
</div>

#### Book List Page
<div align="center">
    <img src="https://gitlab.com/radiophp/bookclub/-/raw/main/Screenshot-BOOK-LIST.png?ref_type=heads&inline=false" alt="Book List Page" width="800px">
</div>

#### Book View Page
<div align="center">
    <img src="https://gitlab.com/radiophp/bookclub/-/raw/main/Screenshot-BOOK-VIEW.png?ref_type=heads&inline=false" alt="Book View Page" width="800px">
</div>
## Technologies Used

- **Supabase**: For database management and user authentication.
- **Imgur**: For image hosting.
- **Bootstrap 5**: For responsive design and styling.
- **Pure JavaScript**: For all client-side logic.

## Database Schema

The database schema is designed to handle books, comments, and messages, with relationships to user accounts managed by Supabase. Below is the schema diagram:
## Database Schema

<div align="center">
    <img src="https://gitlab.com/radiophp/bookclub/-/raw/main/Screenshot-DATABASE.png?ref_type=heads&inline=false" alt="Database Schema" width="800px">
</div>


### Tables

1. **books**
   - `id`: UUID, primary key
   - `title`: text
   - `author`: text
   - `category`: text
   - `image_url`: text
   - `created_by`: UUID, references `auth.users.id`
   - `created_at`: timestamp
   - `description`: text

2. **comments**
   - `id`: UUID, primary key
   - `book_id`: UUID, references `books.id`
   - `user_id`: UUID, references `auth.users.id`
   - `comment`: text
   - `rating`: integer
   - `created_at`: timestamp

3. **messages**
   - `id`: UUID, primary key
   - `user_id`: UUID, references `auth.users.id`
   - `content`: text
   - `created_at`: timestamp
   - `image_url`: text

## Project Structure

- **home.html**: The main page which includes the latest books and chat room.
- **login.html**: The login page.
- **register.html**: The registration page.
- **addbook.html**: The page for adding a new book.
- **booklist.html**: The page showing a list of all books.
- **bookview.html**: The page showing details of a specific book along with comments and ratings.

### JavaScript Files

- **main.js**: Contains the main logic for user authentication and initialization.
- **auth.js**: Handles authentication-related operations.
- **books.js**: Manages book-related operations.
- **addbook.js**: Handles the logic for adding a new book.
- **booklist.js**: Fetches and displays the list of books.
- **bookview.js**: Fetches and displays book details, comments, and handles adding new comments.
- **chat.js**: Manages the chat room functionality.

## How It Works

### User Registration and Login

- Users can register on the `register.html` page. The registration form collects the user's email and password.
- Upon successful registration, the user is redirected to the login page.
- Users can log in on the `login.html` page. Upon successful login, the user is redirected to the main page.

### Adding a Book

- Logged-in users can add a new book on the `addbook.html` page.
- The form collects the book's title, author, category, description, and an image.
- The image is uploaded to Imgur, and the returned URL is stored in the Supabase database along with the other book details.

### Viewing Books

- All books are displayed on the `booklist.html` page.
- Users can click on a book to view its details on the `bookview.html` page, which also displays user comments and ratings.

### Comments and Ratings

- On the `bookview.html` page, users can add comments and rate the book.
- The comments section displays each comment with the user's email, comment, rating, and the date.

### Chat Room

- The chat room is accessible from the main page.
- Users can send messages and images, which are displayed in real-time.

##Conclusion
This Book Club project demonstrates the use of online databases (Supabase) and image hosting (Imgur) with a pure JavaScript frontend. The application provides a comprehensive platform for managing books, user interactions through comments and ratings, and a public chat room. Feel free to explore and extend the functionality as needed.