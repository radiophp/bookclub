import { createClient } from 'https://cdn.jsdelivr.net/npm/@supabase/supabase-js/+esm';

const supabaseUrl = 'https://pzyvvwzzxxfwcmmktxdh.supabase.co';
const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InB6eXZ2d3p6eHhmd2NtbWt0eGRoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTc4Njc1NzIsImV4cCI6MjAzMzQ0MzU3Mn0.oxSOqjIGBM9op7CmRmPrAH17G_SbTA4C7ewpGO0Jk_0';
const supabase = createClient(supabaseUrl, supabaseKey);

document.addEventListener('DOMContentLoaded', async () => {
    const params = new URLSearchParams(window.location.search);
    const bookId = params.get('id');
    const bookTitle = document.getElementById('bookTitle');
    const bookAuthor = document.getElementById('bookAuthor');
    const bookCategory = document.getElementById('bookCategory');
    const bookDescription = document.getElementById('bookDescription');
    const bookScore = document.getElementById('bookScore');
    const bookImage = document.getElementById('bookImage');
    const commentsSection = document.getElementById('commentsSection');
    const addCommentForm = document.getElementById('addCommentForm');

    if (!bookId) {
        alert('No book ID provided');
        return;
    }

    // Fetch book details
    const { data: book, error: bookError } = await supabase
        .from('books')
        .select('*')
        .eq('id', bookId)
        .single();

    if (bookError) {
        alert('Error fetching book: ' + bookError.message);
        return;
    }

    bookTitle.textContent = book.title;
    bookAuthor.textContent = 'Author: ' + book.author;
    bookCategory.textContent = 'Category: ' + book.category;
    bookDescription.textContent = 'Description: ' + book.description;
    bookImage.src = book.image_url;

    // Fetch comments and calculate average score
    const { data: comments, error: commentsError } = await supabase
        .from('comments_with_users')
        .select('*')
        .eq('book_id', bookId);

    if (commentsError) {
        alert('Error fetching comments: ' + commentsError.message);
        return;
    }

    if (comments.length > 0) {
        let totalScore = 0;
        comments.forEach(comment => {
            totalScore += comment.rating;
            const commentElement = document.createElement('div');
            commentElement.className = 'mb-3';
            commentElement.innerHTML = `
                <div class="card">
                    <div class="card-body" style="align-items:first baseline">
                        <p><b>${comment.email}</b>: ${comment.comment}</p>
                        <small>Rating: ${comment.rating}</small> <br>
                        <small>Date: ${new Date(comment.created_at).toLocaleString()}</small>
                    </div>
                </div>
            `;
            commentsSection.appendChild(commentElement);
        });
        const averageScore = totalScore / comments.length;
        bookScore.textContent = 'Average Score: ' + averageScore.toFixed(2);
    } else {
        bookScore.textContent = 'Average Score: No ratings yet';
    }

    // Handle adding a new comment
    addCommentForm.addEventListener('submit', async (event) => {
        event.preventDefault();
        const comment = document.getElementById('comment').value;
        const rating = document.querySelector('input[name="rating"]:checked').value;

        const { data: userData, error: userError } = await supabase.auth.getUser();

        if (userError) {
            alert('Error fetching user: ' + userError.message);
            return;
        }

        const { data, error } = await supabase
            .from('comments')
            .insert([
                { book_id: bookId, user_id: userData.user.id, comment: comment, rating: parseInt(rating) }
            ]);

        if (error) {
            alert('Error adding comment: ' + error.message);
        } else {
            alert('Comment added successfully');
            window.location.reload();
        }
    });
});
