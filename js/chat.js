import { createClient } from 'https://cdn.jsdelivr.net/npm/@supabase/supabase-js/+esm';

const supabaseUrl = 'https://pzyvvwzzxxfwcmmktxdh.supabase.co';
const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InB6eXZ2d3p6eHhmd2NtbWt0eGRoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTc4Njc1NzIsImV4cCI6MjAzMzQ0MzU3Mn0.oxSOqjIGBM9op7CmRmPrAH17G_SbTA4C7ewpGO0Jk_0';
const supabase = createClient(supabaseUrl, supabaseKey);

const cloudName = 'bookclub-inanloo';  // Replace with your Cloudinary cloud name
const unsignedUploadPreset = 'unsigned_preset';  // Replace with your Cloudinary unsigned upload preset

document.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('form');
    const input = document.getElementById('input');
    const imageInput = document.getElementById('imageInput');
    const messages = document.getElementById('messages');

    async function fetchMessages() {
        const { data: messagesData, error } = await supabase
            .from('messages_with_user')
            .select('*')
            .order('created_at', { ascending: true });

        if (error) {
            console.error('Error fetching messages:', error.message);
            return;
        }

        messages.innerHTML = '';
        messagesData.forEach(message => {
            const item = document.createElement('div');
            if (message.image_url) {
                item.innerHTML = `<strong>${message.user_email}</strong>: <br><img src="${message.image_url}" alt="Image" style="max-width: 100%;">`;
            } else {
                item.innerHTML = `<strong>${message.user_email}</strong>: ${message.content}`;
            }
            messages.appendChild(item);

        });
        messages.scrollTop = messages.scrollHeight;
        //
    }

    async function uploadImage(file) {
        const formData = new FormData();
        formData.append('file', file);
        formData.append('upload_preset', unsignedUploadPreset);

        const response = await fetch(`https://api.cloudinary.com/v1_1/${cloudName}/upload`, {
            method: 'POST',
            body: formData
        });

        const data = await response.json();

        if (data.secure_url) {
            return data.secure_url;
        } else {
            console.error('Error uploading image:', data);
            return null;
        }
    }

    async function sendMessage(content, imageUrl = null) {
        const { data: userData, error: userError } = await supabase.auth.getUser();

        if (userError) {
            alert('Error fetching user: ' + userError.message);
            return;
        }

        const { error } = await supabase
            .from('messages')
            .insert([{ user_id: userData.user.id, content: content, image_url: imageUrl }]);

        if (error) {
            console.error('Error sending message:', error.message);
        }
    }

    form.addEventListener('submit', async (e) => {
        e.preventDefault();
        if (input.value) {
            await sendMessage(input.value);
            input.value = '';
            fetchMessages();
        }
    });

    imageInput.addEventListener('change', async () => {
        const file = imageInput.files[0];
        if (file) {
            const imageUrl = await uploadImage(file);
            if (imageUrl) {
                await sendMessage('', imageUrl);
                fetchMessages();
            }
        }
    });

    setInterval(fetchMessages, 2000); // Fetch messages every 2 seconds

    // Initial fetch
    fetchMessages();
});
