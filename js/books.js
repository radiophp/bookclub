import { createClient } from 'https://cdn.jsdelivr.net/npm/@supabase/supabase-js/+esm';

const supabaseUrl = 'https://pzyvvwzzxxfwcmmktxdh.supabase.co';
const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InB6eXZ2d3p6eHhmd2NtbWt0eGRoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTc4Njc1NzIsImV4cCI6MjAzMzQ0MzU3Mn0.oxSOqjIGBM9op7CmRmPrAH17G_SbTA4C7ewpGO0Jk_0';
const supabase = createClient(supabaseUrl, supabaseKey);

document.addEventListener('DOMContentLoaded', async () => {
    const booksContainer = document.getElementById('books-container');

    // Fetch the latest 20 books
    const { data: books, error } = await supabase
        .from('books')
        .select('*')
        .order('created_at', { ascending: false })
        .limit(20);

    if (error) {
        console.error('Error fetching books:', error);
        return;
    }

    // Display the books in a responsive grid
    books.forEach(book => {
        const bookElement = document.createElement('div');
        bookElement.className = 'col-xs-12 col-md-3 col-lg-2 mb-4';
        bookElement.innerHTML = `
            <a href="bookview.html?id=${book.id}" class="book-link">
                <div class="card h-100">
                    <div class="card-img-top book-image" style="background-image: url('${book.image_url}');"></div>
                    <div class="card-body">
                        <h5 class="card-title">${book.title}</h5>
                    </div>
                </div>
            </a>
        `;
        booksContainer.appendChild(bookElement);
    });
});
