import { createClient } from 'https://cdn.jsdelivr.net/npm/@supabase/supabase-js/+esm';

const supabaseUrl = 'https://pzyvvwzzxxfwcmmktxdh.supabase.co';
const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InB6eXZ2d3p6eHhmd2NtbWt0eGRoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTc4Njc1NzIsImV4cCI6MjAzMzQ0MzU3Mn0.oxSOqjIGBM9op7CmRmPrAH17G_SbTA4C7ewpGO0Jk_0';
const supabase = createClient(supabaseUrl, supabaseKey);

const cloudName = 'bookclub-inanloo';  // Replace with your Cloudinary cloud name
const unsignedUploadPreset = 'unsigned_preset';  // Replace with your Cloudinary unsigned upload preset

document.addEventListener('DOMContentLoaded', () => {
    const addBookForm = document.getElementById('addBookForm');

    addBookForm.addEventListener('submit', async (event) => {
        event.preventDefault();

        const title = document.getElementById('title').value;
        const author = document.getElementById('author').value;
        const category = document.getElementById('category').value;
        const description = document.getElementById('description').value;
        const imageFile = document.getElementById('image').files[0];

        let imageUrl = '';

        if (imageFile) {
            const formData = new FormData();
            formData.append('file', imageFile);
            formData.append('upload_preset', unsignedUploadPreset);

            const response = await fetch(`https://api.cloudinary.com/v1_1/${cloudName}/upload`, {
                method: 'POST',
                body: formData
            });

            const data = await response.json();

            if (data.secure_url) {
                imageUrl = data.secure_url;
            } else {
                alert('Error uploading image');
                return;
            }
        }

        const { data: userData, error: userError } = await supabase.auth.getUser();

        if (userError) {
            alert('Error fetching user: ' + userError.message);
            return;
        }

        const { data, error } = await supabase
            .from('books')
            .insert([
                { title, author, category, description, image_url: imageUrl, created_by: userData.user.id }
            ]);

        if (error) {
            alert('Error adding book: ' + error.message);
        } else {
            alert('Book added successfully');
            // Redirect to the book list page
            window.location.href = 'booklist.html';
        }
    });
});
