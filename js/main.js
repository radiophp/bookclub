import { createClient } from 'https://cdn.jsdelivr.net/npm/@supabase/supabase-js/+esm';

document.addEventListener('DOMContentLoaded', () => {
    const supabaseUrl = 'https://pzyvvwzzxxfwcmmktxdh.supabase.co';
    const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InB6eXZ2d3p6eHhmd2NtbWt0eGRoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTc4Njc1NzIsImV4cCI6MjAzMzQ0MzU3Mn0.oxSOqjIGBM9op7CmRmPrAH17G_SbTA4C7ewpGO0Jk_0';
    const supabase = createClient(supabaseUrl, supabaseKey);

    // Register function
    async function registerUser(event) {
        event.preventDefault();
        const email = document.getElementById('username').value;
        const password = document.getElementById('password').value;

        const { data, error } = await supabase.auth.signUp({
            email: email,
            password: password
        });

        if (error) {
            alert('Error: ' + error.message);
        } else {
            alert('User registered successfully, please confirm your email address before logging in.');
            window.location.href = 'login.html';
        }
    }

    // Login function
    async function loginUser(event) {
        event.preventDefault();
        const email = document.getElementById('username').value;
        const password = document.getElementById('password').value;

        const { data, error } = await supabase.auth.signInWithPassword({
            email: email,
            password: password
        });

        if (error) {
            alert('Error: ' + error.message);
        } else {
            alert('User logged in successfully');
            window.location.href = 'home.html';
        }
    }

    // Attach event listeners to the forms
    document.getElementById('registerForm')?.addEventListener('submit', registerUser);
    document.getElementById('loginForm')?.addEventListener('submit', loginUser);
});
