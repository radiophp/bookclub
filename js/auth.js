import { createClient } from 'https://cdn.jsdelivr.net/npm/@supabase/supabase-js/+esm';

const supabaseUrl = 'https://pzyvvwzzxxfwcmmktxdh.supabase.co';
const supabaseKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InB6eXZ2d3p6eHhmd2NtbWt0eGRoIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTc4Njc1NzIsImV4cCI6MjAzMzQ0MzU3Mn0.oxSOqjIGBM9op7CmRmPrAH17G_SbTA4C7ewpGO0Jk_0';
const supabase = createClient(supabaseUrl, supabaseKey);

$(function(){
    const currentPath = window.location.pathname.split('/').pop();
    const publicPages = ['login.html', 'register.html', 'logout.html'];

    if (!publicPages.includes(currentPath)) {
        supabase.auth.getUser().then(({ data: { user } }) => {
            if (!user) {
                window.location.href = 'login.html';
            }
        });
    }

    $("#menu-placeholder").load("menu.html", async function() {
        const { data: { user } } = await supabase.auth.getUser();
        if (user) {
            $("#loginMenu").hide();
            $("#registerMenu").hide();
            $("#logoutMenu").removeClass('d-none');
        }
    });
});
